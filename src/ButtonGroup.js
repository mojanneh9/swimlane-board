import React, { useState } from "react";
import Button from "./Button";

function ButtonGroup() {
  const buttons = [
    { id: 1, name: "Reports", classNames: ["btn", "icon", "reports"] },
    { id: 2, name: "Edit Board", classNames: ["btn", "icon", "edit"] },
    { id: 3, name: "Standard button", classNames: ["btn"] },
    { id: 4, name: "Standard button", classNames: ["btn"] }
  ];
  const [count, setCount] = useState(0);
  const addOneToCount = () => {
    setCount(count + 1);
  };
  const renderButtons = () => {
    return buttons.map(button => {
      return (
        <Button
          key={button.id}
          name={button.name}
          onSetCount={addOneToCount}
          classNames={button.classNames}
        />
      );
    });
  };

  return (
    <div className="button-group">
      {count}
      <br />
      {renderButtons()}
    </div>
  );
}

export default ButtonGroup;
