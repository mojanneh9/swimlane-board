import React, { useState } from "react";
import SwimLane from "./SwimLane";
import { STORIES } from "./STORIES";

function SwimLaneGroup() {
  const [stories, setStories] = useState(STORIES);
  const swimlanes = [
    {
      id: 1,
      title: "Backlog (A very long title that will cause ellipses to occur)"
    },
    { id: 2, title: "Ready" },
    { id: 3, title: "In Progress" },
    { id: 4, title: "Test" },
    { id: 5, title: "Done" }
  ];
  const renderSwimLanes = () => {
    return swimlanes.map(swimlane => {
      return <SwimLane key={swimlane.id} title={swimlane.title} />;
    });
  };

  return <div className="swimlanes">{renderSwimLanes()}</div>;
}

export default SwimLaneGroup;
