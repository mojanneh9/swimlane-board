import React from "react";

function HeaderTitle(props) {
  return <h2 className="ellipses-text">{props.title}</h2>;
}

export default HeaderTitle;
