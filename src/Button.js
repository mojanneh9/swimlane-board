import React from "react";

function Button(props) {
  const generateClassNames = classNames => {
    return classNames.join(" ");
  };
  return (
    <button
      onClick={props.onSetCount}
      type="button"
      className={generateClassNames(props.classNames)}
    >
      {props.name}
    </button>
  );
}

export default Button;
