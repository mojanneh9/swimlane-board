import React from "react";
import "./App.css";
import Header from "./Header";
import SwimLaneGroup from "./SwimLaneGroup";

function App() {
  return (
    <div className="app">
      <div className="board">
        <Header />
        <SwimLaneGroup />
      </div>
    </div>
  );
}

export default App;
