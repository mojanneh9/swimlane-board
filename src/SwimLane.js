import React from "react";
function SwimLane(props) {
  return (
    <div className="swimlane-container">
      <div className="swimlane-head ellipses-text">{props.title}</div>
      <div className="swimlane-body"></div>
    </div>
  );
}

export default SwimLane;
