import React from "react";
import ButtonGroup from "./ButtonGroup";
import HeaderTitle from "./HeaderTitle";

function Header() {
  const title = "My Swim Lane";

  return (
    <header className="header-bar">
      <HeaderTitle title={title} />
      <ButtonGroup />
    </header>
  );
}

export default Header;
